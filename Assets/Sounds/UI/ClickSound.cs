﻿using UnityEngine;

public class ClickSound : MonoBehaviour
{
    public AudioClip AudioClip;

    public AudioSource AudioSource;


    public void Start()
    {
        gameObject.AddComponent<AudioSource>();
        AudioSource.clip = AudioClip;
        AudioSource.playOnAwake = false;
    }

    public void PlaySound()
    {
        AudioSource.PlayOneShot(AudioClip);
    }
}