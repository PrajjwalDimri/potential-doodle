﻿namespace Inputs
{
    public enum Axis
    {
        Horizontal,
        Vertical
    }


    /// InputManagerAxes Names from Unity Input Manager
    public enum InputManagerAxes
    {
        Horizontal,
        Vertical,
        Fire1,
        Fire2,
        Fire3,
        Handbrake,
        GearUp,
        GearDown,
        Horn,
        HeadLight,
        ChangeCamera,
        RightStickHorizontal,
        RightStickVertical
    }
}