﻿using UnityEngine;

namespace Managers
{
    public static class PlayerPrefsManager
    {
        public static void DeletePlayerPrefKey(FloatValueKey key)
        {
            PlayerPrefs.DeleteKey(key.ToString());
            SavePlayerPrefs();
        }

        public static void DeletePlayerPrefKey(IntValueKey key)
        {
            PlayerPrefs.DeleteKey(key.ToString());
            SavePlayerPrefs();
        }

        public static void DeletePlayerPrefKey(StringValueKey key)
        {
            PlayerPrefs.DeleteKey(key.ToString());
            SavePlayerPrefs();
        }

        public static float GetPlayerPrefFloat(FloatValueKey key)
        {
            return PlayerPrefs.GetFloat(key.ToString(), 0.0f);
        }

        public static int GetPlayerPrefInt(IntValueKey key)
        {
            return PlayerPrefs.GetInt(key.ToString(), 0);
        }

        public static string GetPlayerPrefString(StringValueKey key)
        {
            return PlayerPrefs.GetString(key.ToString(), string.Empty);
        }

        public static bool GetPlayerPrefBool(BoolValueKey key)
        {
            //HACK: Unity doesn't support boolean values.
            return PlayerPrefs.GetInt(key.ToString(), 0) != 0;
        }

        public static void SavePlayerPrefFloat(FloatValueKey key, float value)
        {
            PlayerPrefs.SetFloat(key.ToString(), value);
            SavePlayerPrefs();
        }

        public static void SavePlayerPrefInt(IntValueKey key, int value)
        {
            PlayerPrefs.SetInt(key.ToString(), value);
            SavePlayerPrefs();
        }

        public static void SavePlayerPrefString(StringValueKey key, string value)
        {
            PlayerPrefs.SetString(key.ToString(), value);
            SavePlayerPrefs();
        }

        public static void SavePlayerPrefBool(BoolValueKey key, bool value)
        {
            //HACK: Unity doesn't support boolean values.
            PlayerPrefs.SetInt(key.ToString(), value ? 1 : 0);
            SavePlayerPrefs();
        }

        /// <summary>
        ///     Note: This method is by default called on all the playerprefs methods.
        /// </summary>
        private static void SavePlayerPrefs()
        {
            //TODO: Show some kind of icon while saving
            PlayerPrefs.Save();
        }
    }


    /// <summary>
    ///     Values associated with keys will be stored as Floating Point Value
    /// </summary>
    public enum FloatValueKey
    {
        AudioVolume
    }

    /// <summary>
    ///     Values associated with these keys will be stored as Integer
    /// </summary>
    public enum IntValueKey
    {
        ResolutionIndex,
        TextureQualityIndex,
        QualityValueIndex,
        GearingModeIndex
    }

    /// <summary>
    ///     Values associated with keys will be stored as String
    /// </summary>
    public enum StringValueKey
    {
        Username
    }

    /// <summary>
    ///     Values associated with keys will be stored as String
    /// </summary>
    public enum BoolValueKey
    {
        IsFullScreen
    }
}