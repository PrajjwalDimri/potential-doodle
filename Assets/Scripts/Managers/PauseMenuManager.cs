﻿using UnityEngine;

namespace Managers
{
    public class PauseMenuManager : MonoBehaviour
    {
        public Transform HUDCanvasTransform;
        public Transform PauseTransform;


        // Update is called once per frame
        private void Update()
        {
            // if (!isLocalPlayer) return;

            if (Input.GetKeyDown(KeyCode.Escape))
                LoadPauseMenu();
        }

        public void LoadPauseMenu()
        {
            if (PauseTransform.gameObject.activeInHierarchy == false)
            {
                Time.timeScale = 0;
                HUDCanvasTransform.gameObject.SetActive(false);
                PauseTransform.gameObject.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                PauseTransform.gameObject.SetActive(false);
                HUDCanvasTransform.gameObject.SetActive(true);
            }
        }
    }
}