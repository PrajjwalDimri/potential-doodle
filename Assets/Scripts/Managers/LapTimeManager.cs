﻿using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class LapTimeManager : MonoBehaviour
    {
        public static int MinuteCount;
        public static int SecondCount;
        public static float MilliSecondCount;
        public static string MilliDisplay;
        public GameObject MilliSecBoxGameObject;

        public GameObject MinuteBoxGameObject;
        public GameObject SecondBoxGameObject;

        private void Update()
        {
            MilliSecondCount += Time.deltaTime * 10;
            MilliDisplay = MilliSecondCount.ToString("f0");
            MilliSecBoxGameObject.GetComponent<Text>().text = "" + MilliDisplay;

            if (MilliSecondCount >= 10)
            {
                MilliSecondCount = 0;
                SecondCount += 1;
            }

            if (SecondCount <= 9)
                SecondBoxGameObject.GetComponent<Text>().text = "0" + SecondCount + ".";
            else
                SecondBoxGameObject.GetComponent<Text>().text = "" + SecondCount + ".";


            if (SecondCount >= 60)
            {
                SecondCount = 0;
                MinuteCount += 1;
            }

            if (MinuteCount <= 9)
                MinuteBoxGameObject.GetComponent<Text>().text = "0" + MinuteCount + ":";

            else
                MinuteBoxGameObject.GetComponent<Text>().text = "" + MinuteCount + ":";
        }
    }
}