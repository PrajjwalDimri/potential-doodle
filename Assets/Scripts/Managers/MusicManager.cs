﻿using UI_Scripts;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class MusicManager : MonoBehaviour
    {
        private AudioSource _audioSource;
        private NewTrackNotifier _newTrackNotifier;
        private int _playListIndex;

        public AudioClip[] ElectronicClips;
        public AudioClip[] EpicClips;
        public AudioClip[] RockClips;

        public static MusicManager Instance { get; set; }
        public bool IsPlaying => _audioSource.isPlaying;
        public float CurrentClipVolume => _audioSource.volume;


        private void Awake()
        {
            // Singleton Class
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

            _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.playOnAwake = false;
            _newTrackNotifier = FindObjectOfType<NewTrackNotifier>();

            SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
        }

        private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
        {
            if (_newTrackNotifier == null)
                _newTrackNotifier = FindObjectOfType<NewTrackNotifier>();

            if (_audioSource != null && IsPlaying)
                return;

            var activeSceneName = SceneManager.GetActiveScene().name;

            if (activeSceneName.Equals("MainMenu"))
                PlayElectronicSongs();
            else if (activeSceneName.Equals("Practice Track"))
                PlayRockSongs();
        }

        /// <summary>
        ///     Plays provided clip
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="isLooped"></param>
        /// <param name="volume"></param>
        private void PlayClip(AudioClip clip, bool isLooped = false, float volume = 1f)
        {
            Stop();
            _audioSource.clip = clip;
            _audioSource.loop = isLooped;
            SetVolume(volume);
            _audioSource.Play();

            if (_newTrackNotifier != null)
                _newTrackNotifier.ShowTrackNotification(clip.name);
        }

        /// <summary>
        ///     Sets the volume of background music
        /// </summary>
        /// <param name="volume"></param>
        public void SetVolume(float volume)
        {
            _audioSource.volume = volume;
        }

        /// <summary>
        ///     Stops playing all audio.
        /// </summary>
        public void Stop()
        {
            _audioSource.Stop();
        }

        /// <summary>
        ///     Play Songs from RockClipList
        /// </summary>
        public void PlayRockSongs()
        {
            if (RockClips.Length <= 0)
            {
                Debug.LogError("No Rock Music Clips Provided. Please Check the MusicManager object in inspector.");
                return;
            }

            _playListIndex = Random.Range(0, RockClips.Length - 1);

            PlayClip(RockClips[_playListIndex]);

            Invoke(nameof(PlayRockSongs), _audioSource.clip.length + 2);
        }

        /// <summary>
        ///     Plays songs from ElectronicClipList
        /// </summary>
        public void PlayElectronicSongs()
        {
            if (ElectronicClips.Length <= 0)
            {
                Debug.LogError(
                    "No Electronic Music Clips Provided. Please Check the MusicManager object in inspector.");
                return;
            }

            _playListIndex = Random.Range(0, ElectronicClips.Length - 1);

            PlayClip(ElectronicClips[_playListIndex]);

            Invoke(nameof(PlayElectronicSongs), _audioSource.clip.length + 2);
        }

        /// <summary>
        ///     Plays songs from EpicClipList
        /// </summary>
        public void PlayEpicSongs()
        {
            if (EpicClips.Length <= 0)
            {
                Debug.LogError("No Epic Music Clips Provided. Please Check the MusicManager object in inspector.");
                return;
            }

            _playListIndex = Random.Range(0, EpicClips.Length - 1);

            PlayClip(EpicClips[_playListIndex]);
            Invoke(nameof(PlayEpicSongs), _audioSource.clip.length + 2);
        }
    }
}