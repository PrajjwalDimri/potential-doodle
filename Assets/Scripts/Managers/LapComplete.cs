﻿using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class LapComplete : MonoBehaviour
    {
        public GameObject HalfPointTrig;

        public GameObject LapCompleteTrigger;
        public GameObject MilliDisplay;


        public GameObject MinuteDisplay;
        public GameObject SecondDisplay;


        private void OnTiggerEnter()
        {
            LapCompleteTrigger.SetActive(false);
            HalfPointTrig.SetActive(true);
            if (LapTimeManager.SecondCount <= 9)
                SecondDisplay.GetComponent<Text>().text = "0" + LapTimeManager.SecondCount + ".";
            else
                SecondDisplay.GetComponent<Text>().text = "" + LapTimeManager.SecondCount + ".";


            if (LapTimeManager.MinuteCount <= 9)
                MinuteDisplay.GetComponent<Text>().text = "0" + LapTimeManager.MinuteCount + ":";
            else
                MinuteDisplay.GetComponent<Text>().text = "" + LapTimeManager.MinuteCount + ":";

            MilliDisplay.GetComponent<Text>().text = LapTimeManager.MilliDisplay;
            LapTimeManager.MinuteCount = 0;
            LapTimeManager.SecondCount = 0;
            LapTimeManager.MilliSecondCount = 0;
        }
    }
}