﻿using UnityEngine;

namespace Managers
{
    public class HalfPointTrigger : MonoBehaviour
    {
        public GameObject HalfPointTrig;
        public GameObject LapCompleteTrigger;


        private void OnTriggerEnter()
        {
            LapCompleteTrigger.SetActive(true);
            HalfPointTrig.SetActive(false);
        }
    }
}