﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Managers
{
    public class LevelManager : MonoBehaviour
    {
        public static LevelManager Instance;
        private static float _levelLoadingProgress;

        public GameObject LoadingScreen;
        public Slider ProgressSlider;
        public Text ProgressText;

        private bool _isLoading;


        private void Awake()
        {
            LoadingScreen.SetActive(false);
            // Singleton Pattern

            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            if (!_isLoading) return;

            ProgressSlider.value = _levelLoadingProgress;
            ProgressText.text = $"LOADING {_levelLoadingProgress * 100}%";
        }

        public void LoadLevel(string levelName)
        {
            StartCoroutine(LoadLevelAfterDelayASync(levelName, 0f));
        }

        public void LoadLevel(string levelName, float delayInSeconds)
        {
            StartCoroutine(LoadLevelAfterDelayASync(levelName, delayInSeconds));
        }

        private IEnumerator LoadLevelAfterDelayASync(string levelName, float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds);
            LoadingScreen.SetActive(true);
            _isLoading = true;
            var levelLoadingOperation = SceneManager.LoadSceneAsync(levelName);

            while (!levelLoadingOperation.isDone)
            {
                //Dividing by 9 as load level progress only goes up to 0.9
                _levelLoadingProgress = Mathf.Clamp01(levelLoadingOperation.progress / 0.9f);
                yield return null;
            }

            LoadingScreen.SetActive(false);
            _isLoading = false;
        }

        public void LoadLevelWithFade(string levelName, float delayInSeconds = 0f)
        {
            StartCoroutine(LoadLevelWithFadeAfterDelay(levelName, delayInSeconds));
        }

        private IEnumerator LoadLevelWithFadeAfterDelay(string levelName, float delayInSeconds)
        {
            yield return new WaitForSeconds(delayInSeconds);
            Initiate.Fade(levelName, Color.black, 2.0f);
        }
    }
}