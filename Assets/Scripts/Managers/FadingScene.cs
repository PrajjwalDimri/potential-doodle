﻿using UnityEngine;

namespace Managers
{
    public class FadingScene : MonoBehaviour
    {
        private readonly int _drawDepth = -1000;
        private float _alpha = 1.0f;
        private int _fadedirection = -1;
        public float _fadeSpeed = 0.8f;
        public Texture2D FadeInTexture2D;

        private void OnGUI()
        {
            _alpha += _fadedirection * _fadeSpeed * Time.deltaTime;
            _alpha = Mathf.Clamp01(_alpha);
            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b);
            GUI.depth = _drawDepth;
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeInTexture2D);
        }


        public float BeginFade(int direction)
        {
            _fadedirection = direction;
            return _fadeSpeed;
        }

        private void OnLevelWasChanged()
        {
            BeginFade(-1);
        }
    }
}