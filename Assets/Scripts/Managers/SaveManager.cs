﻿using System.IO;
using Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class SaveManager : MonoBehaviour
    {
        private const string SaveDataFileName = "savedata";
        private const string SaveDataExtension = ".ggez";

        private SaveManager _instance;


        private bool _isDataLoaded;
        private string SaveDataDirectory => Application.dataPath + "/saves/";


        public SaveData SaveData { get; private set; }

        public SaveManager Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = FindObjectOfType<SaveManager>();
                if (_instance != null) return _instance;

                var go = new GameObject(typeof(SaveManager).ToString());
                _instance = go.AddComponent<SaveManager>();
                return _instance;
            }
        }


        private void Awake()
        {
            // Singleton Pattern
            if (Instance != null)
            {
                Destroy(this);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
                SceneManager.sceneLoaded += SceneManager_sceneLoaded;
            }
        }

        private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            if (SaveData == null) LoadSaveData();
            WriteSaveData();
        }

        private string GetSaveDataFilePath()
        {
            return SaveDataDirectory + SaveDataFileName + SaveDataExtension;
        }

        public void LoadSaveData()
        {
            if (_isDataLoaded) return;

            if (File.Exists(GetSaveDataFilePath()))
                SaveData = SaveData.ReadFromFile(GetSaveDataFilePath());
        }

        public void WriteSaveData()
        {
            if (SaveData == null)
                SaveData = new SaveData();
            SaveData.WriteToFile(GetSaveDataFilePath());
        }
    }
}