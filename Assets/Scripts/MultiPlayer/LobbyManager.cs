﻿using Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace MultiPlayer
{
    /// <inheritdoc />
    /// <summary>
    /// Handles Lobby Operation
    /// </summary>
    public class LobbyManager : NetworkLobbyManager
    {
        public override void OnLobbyServerConnect(NetworkConnection conn)
        {
            base.OnLobbyClientConnect(conn);
        }

        public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
        {
            base.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);

            var isHost = conn.connectionId == 0;

            return UIManager.Instance.AddClientToLobby(conn);
        }

        public override void OnLobbyServerDisconnect(NetworkConnection conn)
        {
            base.OnLobbyClientDisconnect(conn);
            UIManager.Instance.RemoveClientFromLobby(conn);
        }

        public override void OnLobbyServerPlayersReady()
        {
            //UIManager.Instance.DeAttachLobbyPlayers();
            base.OnLobbyServerPlayersReady();
        }
    }
}