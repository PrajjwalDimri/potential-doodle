﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace MultiPlayer
{
    public class MultiPlayerNetworkDiscovery : NetworkDiscovery
    {
        private List<string> _ipAddresses;

        private void Awake()
        {
            _ipAddresses = new List<string>();
        }

        public override void OnReceivedBroadcast(string fromAddress, string data)
        {
            base.OnReceivedBroadcast(fromAddress, data);
            if (_ipAddresses.Contains(fromAddress)) return;
            UIManager.Instance.AddBroadCastEntry(fromAddress, data);
            _ipAddresses.Add(fromAddress);
        }
    }
}