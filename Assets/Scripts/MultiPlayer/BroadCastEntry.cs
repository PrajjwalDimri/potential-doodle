﻿using UnityEngine;
using UnityEngine.UI;

namespace MultiPlayer
{
    public class BroadCastEntry : MonoBehaviour
    {
        public Text IpAddressText;

        public Button JoinButton;

        private void Awake()
        {
            JoinButton.onClick.AddListener(() => { UIManager.Instance.DiscoveryJoinButtonClick(IpAddressText); });
        }
    }
}