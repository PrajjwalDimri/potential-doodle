﻿using System;
using System.Collections.Generic;
using Managers;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MultiPlayer
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;
        public GameObject BroadCastEntry;
        public Button HostButton;
        public Button ConnectUsingIp;
        public InputField IpAddressField;
        public GameObject BroadCastEntryParent;
        public GameObject NetworkSearchPanel;
        public GameObject NetworkLobbyPanel;

        public GameObject LobbyPlayerPrefab;
        public GameObject LobbyPlayerParentPrefab;
        private MultiPlayerNetworkDiscovery _multiPlayerNetworkDiscovery;
        private LobbyManager _lobbyManager;

        private int _numberOfRunningGames = 0;
        private int _numberOfLobbyPlayers = 0;

        private void Awake()
        {
            // Singleton Class
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnEnable()
        {
            _multiPlayerNetworkDiscovery = FindObjectOfType<MultiPlayerNetworkDiscovery>();
            _lobbyManager = FindObjectOfType<LobbyManager>();

            NetworkSearchPanel.SetActive(false);
            NetworkLobbyPanel.SetActive(false);
            _multiPlayerNetworkDiscovery.Initialize();

            StartLocalGameSearch();
        }

        /// <summary>
        /// Adds results of network discovery to Panel.
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="data"></param>
        public void AddBroadCastEntry(string fromAddress, string data)
        {
            var entryTransform = BroadCastEntry.GetComponent<RectTransform>();

            var entry = Instantiate(BroadCastEntry);
            entry.transform.parent = BroadCastEntryParent.transform;
            entry.GetComponent<RectTransform>().anchoredPosition = new Vector2(
                entryTransform.anchoredPosition.x,
                entryTransform.anchoredPosition.y - entryTransform.rect.height * _numberOfRunningGames);
            entry.GetComponent<RectTransform>().localScale = Vector3.one;
            entry.GetComponent<BroadCastEntry>().IpAddressText.text = fromAddress;
            _numberOfRunningGames++;
        }

        /// <summary>
        /// Gets Called whenever user clicks on join button of any Network Discovery
        /// </summary>
        /// <param name="ipAddress"></param>
        public void DiscoveryJoinButtonClick(Text ipAddress)
        {
            NetworkLobbyPanel.SetActive(true);
            _lobbyManager.networkAddress = ipAddress.text;
            _lobbyManager.StartClient();
        }

        /// <summary>
        /// Fires up NetworkDiscovery in Client Mode
        /// </summary>
        public void StartLocalGameSearch()
        {
            NetworkSearchPanel.SetActive(true);
            NetworkLobbyPanel.SetActive(false);
            NetworkServer.Reset();
            _multiPlayerNetworkDiscovery.Initialize();
            _multiPlayerNetworkDiscovery.StartAsClient();
        }

        /// <summary>
        /// Stars a LAN Lobby
        /// </summary>
        public void HostGame()
        {
            NetworkLobbyPanel.SetActive(true);
            NetworkSearchPanel.SetActive(false);

            _lobbyManager.StartHost();
            _multiPlayerNetworkDiscovery.StopBroadcast();
            _multiPlayerNetworkDiscovery.Initialize();
            _multiPlayerNetworkDiscovery.StartAsServer();
        }

        public void ConnectToIp()
        {
            //TODO: Add Direct IP Option
        }

        /// <summary>
        /// Adds UI Clients To Lobby.
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public GameObject AddClientToLobby(NetworkConnection conn)
        {
            var lobbyPlayer = Instantiate(LobbyPlayerPrefab);


            var lobbyPlayerTransform = LobbyPlayerPrefab.GetComponent<RectTransform>();


            lobbyPlayer.GetComponent<LobbyPlayer>().AnchoredPosition = new Vector2(
                lobbyPlayerTransform.anchoredPosition.x,
                lobbyPlayerTransform.anchoredPosition.y - lobbyPlayerTransform.rect.height * _numberOfLobbyPlayers);

            lobbyPlayer.GetComponent<LobbyPlayer>().PlayerNameString = conn.address;
            lobbyPlayer.GetComponent<LobbyPlayer>().ConnectionId = conn.connectionId;
            _numberOfLobbyPlayers++;
            return lobbyPlayer;
        }

        /// <summary>
        /// Removes clients from lobby and maintains the correct order and spacing between remaining clients.
        /// </summary>
        /// <param name="conn"></param>
        public void RemoveClientFromLobby(NetworkConnection conn)
        {
            var lobbyPlayers = FindObjectsOfType<LobbyPlayer>();
            var length = 0;
            var lobbyPlayerTransform = LobbyPlayerPrefab.GetComponent<RectTransform>();

            foreach (var lobbyPlayer in lobbyPlayers)
            {
                if (lobbyPlayer.ConnectionId.Equals(conn.connectionId))
                {
                    Destroy(lobbyPlayer);
                    length--;
                }
                else
                {
                    lobbyPlayer.GetComponent<RectTransform>().anchoredPosition = new Vector2(
                        lobbyPlayerTransform.anchoredPosition.x,
                        lobbyPlayerTransform.anchoredPosition.y - lobbyPlayerTransform.rect.height * length);
                }

                length++;
            }

            _numberOfLobbyPlayers--;
        }

        /// <summary>
        /// Exits Lobby To Main Menu
        /// </summary>
        public void ExitLobby()
        {
            LevelManager.Instance.LoadLevel("MainMenu", 0);
        }

        /// <summary>
        /// Detaches the lobby player objects from their parents so that they don't get destroyed 
        /// while the game switches scenes.
        /// </summary>
        public void DeAttachLobbyPlayers()
        {
            foreach (var lobbyPlayer in FindObjectsOfType<LobbyPlayer>())
            {
                lobbyPlayer.gameObject.transform.parent = null;
                DontDestroyOnLoad(lobbyPlayer);
            }
        }
    }
}