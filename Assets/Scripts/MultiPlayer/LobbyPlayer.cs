﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace MultiPlayer
{
    public class LobbyPlayer : NetworkBehaviour
    {
        private Text _playerName;

        [SyncVar] public string PlayerNameString;
        [SyncVar] public int ConnectionId;
        [SyncVar] public Vector2 AnchoredPosition;

        private Button _readyButton;

        public NetworkLobbyPlayer NetworkLobbyPlayer;

        public GameObject LobbyPlayerUIPrefab;

        private GameObject _lobbyPlayerUI;

        private void Awake()
        {
            _lobbyPlayerUI = Instantiate(LobbyPlayerUIPrefab);

            NetworkLobbyPlayer.readyToBegin = false;
            var lobbyParent = GameObject.Find("LobbyPlayerParent");
            if (lobbyParent != null)
                _lobbyPlayerUI.transform.parent = lobbyParent.transform;
        }

        private void Start()
        {
            _playerName = _lobbyPlayerUI.GetComponentsInChildren<Text>()[1];
            _readyButton = _lobbyPlayerUI.GetComponentInChildren<Button>();
            _playerName.text = PlayerNameString;
            _readyButton.interactable = isLocalPlayer;
            // HACK: Have to search for button as instance of the button is not working
            _readyButton.onClick.AddListener(ReadyCall);

            _lobbyPlayerUI.GetComponent<RectTransform>().anchoredPosition = AnchoredPosition;
            _lobbyPlayerUI.GetComponent<RectTransform>().localScale = Vector3.one;
        }

        private void ReadyCall()
        {
            NetworkLobbyPlayer.SendReadyToBeginMessage();
            _readyButton.interactable = false;
        }
    }
}