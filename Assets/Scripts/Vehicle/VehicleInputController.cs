﻿using System;
using Inputs;
using Managers;
using UnityEngine;
using UnityEngine.Networking;

namespace Vehicle
{
    [RequireComponent(typeof(VehicleAudio))]
    [RequireComponent(typeof(VehicleCamera))]
    [RequireComponent(typeof(VehicleController))]
    [RequireComponent(typeof(VehicleLights))]
    public class VehicleInputController : NetworkBehaviour
    {
        private GearingMode _gearingMode;
        private VehicleAudio _vehicleAudio;
        private VehicleCamera _vehicleCamera;
        private VehicleController _vehicleController;
        private VehicleLights _vehicleLights;

        private void OnEnable()
        {
            _vehicleController = GetComponent<VehicleController>();
            _vehicleCamera = GetComponent<VehicleCamera>();
            _vehicleAudio = GetComponent<VehicleAudio>();
            _vehicleLights = GetComponent<VehicleLights>();
            _gearingMode = (GearingMode) PlayerPrefsManager.GetPlayerPrefInt(IntValueKey.GearingModeIndex);
        }

        private void Update()
        {
            if (!isLocalPlayer)
            {
                GetComponent<AudioListener>().enabled = false;
                return;
            }


            var acceleration = Input.GetAxis(Axis.Vertical.ToString());
            var steering = Input.GetAxis(Axis.Horizontal.ToString());
            var handBrake = Input.GetAxis(InputManagerAxes.Handbrake.ToString());

            _vehicleController.MoveVehicle(steering, acceleration, acceleration, handBrake);

            GearingController();


            _vehicleAudio.IsHornPlaying = Input.GetButton(InputManagerAxes.Horn.ToString());

            // Vehicle Lights
            if (Input.GetButtonDown(InputManagerAxes.HeadLight.ToString()))
                _vehicleLights.IsHeadLightOn = !_vehicleLights.IsHeadLightOn;

            //Change Camera
            if (Input.GetButtonDown(InputManagerAxes.ChangeCamera.ToString()))
                _vehicleCamera.SwitchCamera();
        }

        private void GearingController()
        {
            switch (_gearingMode)
            {
                case GearingMode.Manual:
                    _vehicleController.AutoReverse = false;
                    if (Input.GetButtonDown(InputManagerAxes.GearUp.ToString()))
                    {
                        _vehicleController.IncreaseGear();
                        _vehicleAudio.IsGearChanging = true;
                    }

                    if (Input.GetButtonDown(InputManagerAxes.GearDown.ToString()))
                    {
                        _vehicleController.DecreaseGear();
                        _vehicleAudio.IsGearChanging = true;
                    }

                    break;

                case GearingMode.Automatic:
                    _vehicleController.AutoReverse = true;
                    break;
                case GearingMode.SemiAutomatic:
                    _vehicleController.AutoReverse = false;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}