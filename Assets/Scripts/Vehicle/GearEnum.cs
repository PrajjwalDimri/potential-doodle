﻿namespace Vehicle
{
    public enum Gear
    {
        Reverse,
        Neutral,
        First,
        Second,
        Third,
        Fourth,
        Fifth
    }

    public enum GearingMode
    {
        Automatic,
        SemiAutomatic,
        Manual
    }
}
// Use this for initialization