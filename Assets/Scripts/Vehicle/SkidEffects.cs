﻿using UnityEngine;

namespace Vehicle
{
    public class SkidEffects : MonoBehaviour
    {
        private readonly float _skitAt = 0.5f;

        public float CurveFriction;
        public float MarkWidth = 0.2f;
        public int Skidding;

        public float SmokeDepth = 0.4f;


        private void Update()
        {
            WheelHit hit;


            GetComponent<WheelCollider>().GetGroundHit(out hit);
            CurveFriction = Mathf.Abs(hit.sidewaysSlip);
            if (CurveFriction >= _skitAt)
                GetComponent<TrailRenderer>().enabled = true;
            else
                GetComponent<TrailRenderer>().enabled = false;
        }
    }
}