﻿using UnityEngine;
using UnityEngine.Networking;

namespace Vehicle
{
    [RequireComponent(typeof(VehicleController))]
    public class VehicleAudio : NetworkBehaviour
    {
        private AudioSource _engineIdlePlayer;
        private AudioSource _hornPlayer;
        private float _rpm;
        private VehicleController _vehicleController;
        private AudioSource _vehicleGearChangePlayer;
        public bool IsGearChanging;

        public bool IsHornPlaying;

        public AudioClip VehicleBrakeClip;
        public AudioClip VehicleEngineIdleClip;
        public AudioClip VehicleGearChangeClip;
        public AudioClip VehicleHornClip;
        public AudioClip VehicleSkidClip;

        private void Update()
        {
            if (!isLocalPlayer) return;

            _rpm = _vehicleController.EngineRpm;

            _hornPlayer.volume = IsHornPlaying ? 1f : 0f;

            _engineIdlePlayer.volume = 1f;

            if (IsGearChanging)
            {
                _rpm = 0;
                IsGearChanging = false;
                _vehicleGearChangePlayer.volume = 1;
                _vehicleGearChangePlayer.Play();
            }

            _engineIdlePlayer.pitch = Mathf.Lerp(0.3f, 1.2f, Mathf.Abs(_rpm) / 100 * Time.deltaTime);
        }

        private void Start()
        {
            _vehicleController = GetComponent<VehicleController>();
            _engineIdlePlayer = SetupAudioSource(VehicleEngineIdleClip, true, true);
            _hornPlayer = SetupAudioSource(VehicleHornClip, true, true);
            _vehicleGearChangePlayer = SetupAudioSource(VehicleGearChangeClip, false, true);
        }

        private void StopAllSounds()
        {
            foreach (var audioSource in GetComponents<AudioSource>())
                Destroy(audioSource);
        }

        private AudioSource SetupAudioSource(AudioClip clip, bool isLooped, bool is3D)
        {
            var audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.volume = 0;
            audioSource.loop = isLooped;
            audioSource.spatialBlend = is3D ? 1 : 0;
            audioSource.time = Random.Range(0f, clip.length);
            if (isLooped)
                audioSource.Play();
            return audioSource;
        }
    }
}