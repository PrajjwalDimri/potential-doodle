﻿using UnityEngine;
using UnityEngine.UI;

namespace Vehicle
{
    public class SpeedShow : MonoBehaviour
    {
        public GameObject SpeedText;


        // Update is called once per frame
        private void Update()
        {
            SpeedText.GetComponent<Text>().text = VehicleController.CurrentSpeed.ToString("f0") + "Kmph";
        }
    }
}