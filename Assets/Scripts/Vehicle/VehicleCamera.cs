﻿using UnityEngine;
using UnityEngine.Networking;

namespace Vehicle
{
    public class VehicleCamera : NetworkBehaviour
    {
        private int _currentCameraIndex;

        public Camera[] Cameras;

        private void Awake()
        {
            DisableAllCameras();
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            Cameras[_currentCameraIndex].enabled = true;
        }

        public void SwitchCamera()
        {
            if (!isLocalPlayer) return;

            if (_currentCameraIndex >= Cameras.Length - 1)
                _currentCameraIndex = -1;

            _currentCameraIndex++;
            DisableAllCameras();
            Cameras[_currentCameraIndex].enabled = true;
        }

        private void DisableAllCameras()
        {
            foreach (var cameraObject in Cameras)
                cameraObject.enabled = false;
        }
    }
}