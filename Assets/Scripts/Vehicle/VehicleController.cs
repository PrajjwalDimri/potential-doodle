﻿using UnityEngine;
using UnityEngine.Networking;

namespace Vehicle
{
    public class VehicleController : NetworkBehaviour
    {
        public static float CurrentSpeed;
        private float _forwardFriction;
        private float _maxHandbrakeTorque;
        private float _sideWayFriction;
        private float _slipForwardFriction;
        private float _slipsidewayFriction;
        private float _targetAngle;
        private VehicleLights _vehicleLights;
        private WheelCollider[] _wheelColliders;
        public bool AutoReverse;


        public Gear CurrentGear = Gear.First;
        public float DifferentialRatio;
        public float EngineRpm;
        public AnimationCurve EngineTorqueCurve;

        public float[] GearRatio =
        {
            2.92f, 0, 2.66f, 1.78f, 1.30f, 1.1f
        };

        public float MaxAcceleration;
        public float MaxBrakingAcceleration;
        public float MaxEngineRpm = 10000f;

        public int[] MaxSpeedPerGear =
        {
            20,
            0,
            40,
            70,
            130,
            170
        };

        [Range(20, 40)] public float MaxSteerAngle;
        [Range(10, 1000)] public int MinEngineRpm = 10;
        public GameObject Spark;
        public int SteeringRotationSpeed = 10;
        public int TotalGear;
        [Range(0, 1)] public float TransmissionEfficiency = 0.3f;

        public GameObject WheelMeshFrontLeft;
        public GameObject WheelMeshFrontRight;
        public GameObject WheelMeshRearLeft;
        public GameObject WheelMeshRearRight;

        public bool IsReverse => CurrentGear <= 0;

        public void Start()
        {
            FrictionValues();
        }

        public void Update()
        {
            if (!isLocalPlayer) return;

            CurrentSpeed = EngineRpm / MaxEngineRpm * MaxSpeedPerGear[(int) CurrentGear];
            EngineRpm = Mathf.Clamp(EngineRpm, 0f, MaxEngineRpm);
            CurrentSpeed = Mathf.Clamp(CurrentSpeed, 0f, 200f);
            _targetAngle = Mathf.Lerp(_targetAngle, Mathf.Abs(CurrentSpeed), Time.deltaTime);
        }

        private void FrictionValues()
        {
            _forwardFriction = _wheelColliders[0].forwardFriction.stiffness;
            _sideWayFriction = _wheelColliders[0].sidewaysFriction.stiffness;
            _slipForwardFriction = 0.04f;
            _slipsidewayFriction = 0.08f;
        }

        private void Awake()
        {
            _wheelColliders = GetComponentsInChildren<WheelCollider>();
            _maxHandbrakeTorque = float.MaxValue;
            _vehicleLights = GetComponent<VehicleLights>();
        }


        public void MoveVehicle(float steering, float acceleration, float footBrake, float handBrake)
        {
            steering = Mathf.Clamp(steering, -1, 1);
            acceleration = Mathf.Clamp(acceleration, 0, 1);
            footBrake = Mathf.Clamp(footBrake, -1, 0);
            handBrake = Mathf.Clamp(handBrake, 0, 1);

            if (Mathf.Abs(_wheelColliders[2].rpm) < 1f && acceleration > 0f && EngineRpm < MinEngineRpm)
                EngineRpm = MinEngineRpm;

            if (AutoReverse)
            {
                if (Mathf.Abs(_wheelColliders[2].rpm) < 10f && footBrake < 0f)
                    CurrentGear = Gear.Reverse;

                else if (Mathf.Abs(_wheelColliders[2].rpm) < 10f && footBrake >= 0f)
                    CurrentGear = Gear.First;

                ApplyAcceleration(IsReverse ? -footBrake : acceleration);
                ApplyBrake(IsReverse ? acceleration : footBrake, handBrake);

                CalculateEngineRpm(IsReverse ? footBrake : acceleration);
            }
            else
            {
                ApplyAcceleration(acceleration);
                ApplyBrake(footBrake, handBrake);
                CalculateEngineRpm(IsReverse ? -acceleration : acceleration);
            }


            ApplySteeringToWheelColliders(steering, SteeringRotationSpeed);
            UpdateWheelMeshRotation();
        }


        private void ApplyAcceleration(float acceleration)
        {
            _vehicleLights.IsVehicleInReverse = IsReverse;
            _wheelColliders[2].motorTorque = CalculateMaxMotorTorque() * acceleration;
            _wheelColliders[3].motorTorque = _wheelColliders[2].motorTorque;
        }

        private void CalculateEngineRpm(float acceleration)
        {
            EngineRpm = acceleration * 1000 * GearRatio[(int) CurrentGear] * DifferentialRatio;
        }

        private float CalculateMaxMotorTorque()
        {
            var engineTorque = EngineTorqueCurve.Evaluate(EngineRpm);
            return engineTorque * GearRatio[(int) CurrentGear] * DifferentialRatio *
                   (1 - TransmissionEfficiency);
        }


        private void ApplyBrake(float footBrake, float handBrake)
        {
            var handBrakeTorque = handBrake * _maxHandbrakeTorque;
            var footBrakeTorque = Mathf.Abs(footBrake * MaxBrakingAcceleration);
            _wheelColliders[0].brakeTorque = footBrakeTorque;
            _wheelColliders[1].brakeTorque = footBrakeTorque;
            _wheelColliders[2].brakeTorque = handBrakeTorque;
            _wheelColliders[3].brakeTorque = handBrakeTorque;

            if (GetComponent<Rigidbody>().velocity.magnitude > 1)
                SetSlip(_slipForwardFriction, _slipsidewayFriction);
            else
                SetSlip(1, 1);

            if (handBrake > 0f || footBrakeTorque > 0f)
                _vehicleLights.IsBrakeApplied = true;
            else
                _vehicleLights.IsBrakeApplied = false;
        }

        private void UpdateWheelMeshRotation()
        {
            // Vertical Rotation

            WheelMeshFrontLeft.transform
                .Rotate(_wheelColliders[0].rpm / 60 * 360 * Time.fixedDeltaTime, 0, 0);

            WheelMeshFrontRight.transform
                .Rotate(_wheelColliders[1].rpm / 60 * 360 * Time.fixedDeltaTime, 0, 0);

            WheelMeshRearLeft.transform
                .Rotate(_wheelColliders[2].rpm / 60 * 360 * Time.fixedDeltaTime, 0, 0);

            WheelMeshRearRight.transform
                .Rotate(_wheelColliders[3].rpm / 60 * 360 * Time.fixedDeltaTime, 0, 0);

            // Horizontal Rotation
            WheelMeshFrontLeft.transform.localEulerAngles = new Vector3(WheelMeshFrontLeft.transform.localEulerAngles.x,
                180 + _wheelColliders[0].steerAngle, 0);
            WheelMeshFrontRight.transform.localEulerAngles = new Vector3(
                WheelMeshFrontRight.transform.localEulerAngles.x,
                _wheelColliders[1].steerAngle, 0);
        }

        private void ApplySteeringToWheelColliders(float steering, float steeringRotationSpeed)
        {
            // Assuming the front wheel colliders are number 1 and 2 child.
            _wheelColliders[0].steerAngle = Mathf.Lerp(_wheelColliders[0].steerAngle, steering * MaxSteerAngle,
                Time.deltaTime * steeringRotationSpeed);
            _wheelColliders[1].steerAngle = _wheelColliders[0].steerAngle;
        }


        public void IncreaseGear()
        {
            //TODO: Game is crashing on the final gear
            if ((int) CurrentGear < GearRatio.Length - 2)
                CurrentGear++;
        }


        public void DecreaseGear()
        {
            if (CurrentGear <= 0) return;
            CurrentGear--;
            EngineRpm = 0;
        }

        private void SetSlip(float currentForwardFriction, float currentSidewayFriction)
        {
            var wheelFrictionCurve1 = _wheelColliders[2].forwardFriction;
            wheelFrictionCurve1.stiffness = currentForwardFriction;
            var wheelFrictionCurve2 = _wheelColliders[2].sidewaysFriction;
            wheelFrictionCurve2.stiffness = currentSidewayFriction;
            var wheelFrictionCurve3 = _wheelColliders[3].forwardFriction;
            wheelFrictionCurve3.stiffness = currentForwardFriction;
            var wheelFrictionCurve4 = _wheelColliders[3].sidewaysFriction;
            wheelFrictionCurve4.stiffness = currentSidewayFriction;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!isLocalPlayer) return;

            if (other.transform != transform && other.contacts.Length != 0)
                for (var i = 0; i < other.contacts.Length; i++)
                {
                    var clone = Instantiate(Spark, other.contacts[i].point, Quaternion.identity);
                    Destroy(clone, 2f);
                }
        }
    }
}