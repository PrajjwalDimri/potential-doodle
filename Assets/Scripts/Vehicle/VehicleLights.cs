﻿using UnityEngine;
using UnityEngine.Networking;

namespace Vehicle
{
    public class VehicleLights : NetworkBehaviour
    {
        public Light[] BrakeLights;
        public Light[] HeadLights;
        public bool IsBrakeApplied;
        public bool IsHeadLightOn;
        public bool IsVehicleInReverse;
        public Light[] ReverseLights;


        private void Update()
        {
            if (!isLocalPlayer) return;
            // Reverse-Lights
            if (IsVehicleInReverse)
                foreach (var reverseLight in ReverseLights)
                    reverseLight.intensity = 1.5f;
            else
                foreach (var reverseLight in ReverseLights)
                    reverseLight.intensity = 0f;

            // HeadLights
            if (IsHeadLightOn)
            {
                foreach (var headLight in HeadLights)
                    headLight.intensity = 2.5f;

                // Brake-Lights
                if (IsBrakeApplied)
                    foreach (var brakeLight in BrakeLights)
                        brakeLight.intensity = 3f;
                else
                    foreach (var brakeLight in BrakeLights)
                        brakeLight.intensity = 1f;
            }
            else
            {
                foreach (var headLight in HeadLights)
                    headLight.intensity = 0f;

                // Brake-Lights
                if (IsBrakeApplied)
                    foreach (var brakeLight in BrakeLights)
                        brakeLight.intensity = 1.5f;
                else
                    foreach (var brakeLight in BrakeLights)
                        brakeLight.intensity = 0f;
            }
        }
    }
}