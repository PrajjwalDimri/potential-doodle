﻿using UnityEngine;
using UnityEngine.Networking;

namespace Cameras
{
    public class VehicleThirdPerson : MonoBehaviour
    {
        public GameObject CameraFollowObject;
        public float DistanceFromCar = 4f;
        public float Height = 1.2f;

        private void LateUpdate()
        {
            if (CameraFollowObject == null)
            {
                Debug.LogError("No Camera Follow Object was found in the scene");
                return;
            }

            Cursor.visible = false;

            var wantedRotationAngle = CameraFollowObject.transform.eulerAngles.y;

            var currentRotationAngle = transform.eulerAngles.y;

            currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, 2 * Time.deltaTime);

            var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

            transform.position = CameraFollowObject.transform.position;
            transform.position -= currentRotation * Vector3.forward * DistanceFromCar;

            transform.position = new Vector3(transform.position.x, Height, transform.position.z);

            transform.LookAt(CameraFollowObject.transform);
        }
    }
}