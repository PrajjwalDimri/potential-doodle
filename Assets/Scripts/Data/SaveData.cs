﻿using System.IO;
using UnityEngine;

namespace Data
{
    public class SaveData
    {
        public void WriteToFile(string filePath)
        {
            var json = JsonUtility.ToJson(this, true);
            File.WriteAllText(filePath, json);
        }

        public SaveData ReadFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                Debug.LogError($"\n SaveFile at {filePath} Not Found \n");
                return new SaveData();
            }
            var contents = File.ReadAllText(filePath);

            if (string.IsNullOrEmpty(contents))
            {
                Debug.LogError($"\n SaveFile at {filePath} is empty");
                return new SaveData();
            }
            return JsonUtility.FromJson<SaveData>(contents);
        }
    }
}