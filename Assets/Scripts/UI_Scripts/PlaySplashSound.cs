﻿using UnityEngine;

namespace UI_Scripts
{
    public class PlaySplashSound : MonoBehaviour
    {
        public AudioClip SplashClip;

        private void Awake()
        {
            var audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = 0;
            audioSource.clip = SplashClip;
            audioSource.Play();
        }
    }
}