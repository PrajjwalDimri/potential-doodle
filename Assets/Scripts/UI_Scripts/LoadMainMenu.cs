﻿using Managers;
using UnityEngine;

namespace UI_Scripts
{
    public class LoadMainMenu : MonoBehaviour
    {
        public string LevelName = "MainMenu";


        private void Start()
        {
            LevelManager.Instance.LoadLevelWithFade(LevelName, 6f);
        }
    }
}