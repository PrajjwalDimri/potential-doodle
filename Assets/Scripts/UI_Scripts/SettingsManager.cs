﻿using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI_Scripts
{
    public class SettingsManager : MonoBehaviour
    {
        public Button ApplyButton;
        public Button DefaultButton;
        public Toggle FullScreenToggle;
        public GameSettings GameSettings;
        public Dropdown GearingModeDropDown;
        public Slider MusicVolumeSlider;
        public Dropdown QualityDropDown;
        public Dropdown ResolutionDropDown;


        public Resolution[] Resolutions;


        private void OnEnable()
        {
            GameSettings = new GameSettings();
            FullScreenToggle.onValueChanged.AddListener(delegate { OnFullScreenToggle(); });
            ResolutionDropDown.onValueChanged.AddListener(delegate { OnResolutionChanged(); });
            GearingModeDropDown.onValueChanged.AddListener(delegate { OnGearingModeChanged(); });
            QualityDropDown.onValueChanged.AddListener(delegate { OnQualityChanged(); });
            MusicVolumeSlider.onValueChanged.AddListener(delegate { OnAudioVolumeChanged(); });
            ApplyButton.onClick.AddListener(SaveSettings);
            DefaultButton.onClick.AddListener(LoadDefaultSettings);
            Resolutions = Screen.resolutions;

            foreach (var resolution in Resolutions)
                ResolutionDropDown.options.Add(new Dropdown.OptionData(resolution.ToString()));

            foreach (var qualityName in QualitySettings.names)
                QualityDropDown.options.Add(new Dropdown.OptionData(qualityName));


            LoadSettings();
        }


        private void OnFullScreenToggle()
        {
            GameSettings.IsFullScreen = Screen.fullScreen = FullScreenToggle.isOn;
        }

        private void OnResolutionChanged()
        {
            Screen.SetResolution(Resolutions[ResolutionDropDown.value].width,
                Resolutions[ResolutionDropDown.value].height, Screen.fullScreen);
            GameSettings.ResolutionIndex = ResolutionDropDown.value;
        }

        private void OnGearingModeChanged()
        {
            GameSettings.GearingMode = GearingModeDropDown.value;
        }

        private void OnQualityChanged()
        {
            QualitySettings.SetQualityLevel(QualityDropDown.value);
            GameSettings.QualityValueIndex = QualityDropDown.value;
        }

        private void OnAudioVolumeChanged()
        {
            GameSettings.AudioVolume = MusicVolumeSlider.value;
            AudioListener.volume = GameSettings.AudioVolume;
        }

        private void SaveSettings()
        {
            PlayerPrefsManager.SavePlayerPrefInt(IntValueKey.QualityValueIndex, GameSettings.QualityValueIndex);
            PlayerPrefsManager.SavePlayerPrefInt(IntValueKey.ResolutionIndex, GameSettings.ResolutionIndex);
            PlayerPrefsManager.SavePlayerPrefInt(IntValueKey.GearingModeIndex, GameSettings.GearingMode);
            PlayerPrefsManager.SavePlayerPrefFloat(FloatValueKey.AudioVolume, GameSettings.AudioVolume);
            PlayerPrefsManager.SavePlayerPrefBool(BoolValueKey.IsFullScreen, GameSettings.IsFullScreen);
        }

        private void LoadSettings()
        {
            if (!PlayerPrefs.HasKey(IntValueKey.ResolutionIndex.ToString()))
            {
                LoadDefaultSettings();
                SaveSettings();
                return;
            }
            GameSettings.QualityValueIndex = PlayerPrefsManager.GetPlayerPrefInt(IntValueKey.QualityValueIndex);
            GameSettings.ResolutionIndex = PlayerPrefsManager.GetPlayerPrefInt(IntValueKey.ResolutionIndex);
            GameSettings.GearingMode = PlayerPrefsManager.GetPlayerPrefInt(IntValueKey.GearingModeIndex);
            GameSettings.AudioVolume = PlayerPrefsManager.GetPlayerPrefFloat(FloatValueKey.AudioVolume);
            GameSettings.IsFullScreen = PlayerPrefsManager.GetPlayerPrefBool(BoolValueKey.IsFullScreen);
            RefreshValues();
        }

        private void RefreshValues()
        {
            FullScreenToggle.isOn = GameSettings.IsFullScreen;
            ResolutionDropDown.value = GameSettings.ResolutionIndex;
            GearingModeDropDown.value = GameSettings.GearingMode;
            MusicVolumeSlider.value = GameSettings.AudioVolume;
            QualityDropDown.value = GameSettings.QualityValueIndex;
        }

        private void LoadDefaultSettings()
        {
            GameSettings.QualityValueIndex = QualityDropDown.options.Count - 1;
            GameSettings.IsFullScreen = true;
            GameSettings.AudioVolume = MusicVolumeSlider.maxValue;
            GameSettings.ResolutionIndex = Resolutions.Length - 1;
            GameSettings.GearingMode = 0;
            SaveSettings();
            RefreshValues();
        }
    }
}