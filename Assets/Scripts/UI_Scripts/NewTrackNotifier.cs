﻿using TMPro;
using UnityEngine;

namespace UI_Scripts
{
    public class NewTrackNotifier : MonoBehaviour
    {
        private Animator _animator;
        private TextMeshProUGUI _trackText;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _trackText = GetComponentInChildren<TextMeshProUGUI>();
        }

        public void ShowTrackNotification(string trackName)
        {
            _trackText.text = trackName;
            _animator.SetTrigger("ShowNotification");
        }
    }
}