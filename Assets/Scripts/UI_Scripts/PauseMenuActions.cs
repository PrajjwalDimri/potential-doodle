﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI_Scripts
{
    public class PauseMenuActions : MonoBehaviour
    {
        public Transform HudTransform;
        public Button OptionsButton;
        public Transform OptionsMenuTransform;
        public Transform PauseMenuTransform;
        public Button QuitButton;
        public Button RestartButton;
        public Button ResumeButton;


        public void OnEnable()
        {
            ResumeButton.onClick.AddListener(OnResumeButtonClick);
            RestartButton.onClick.AddListener(OnRestartButtonClick);
            OptionsButton.onClick.AddListener(OnOptionsButtonClick);
            QuitButton.onClick.AddListener(OnQuitButtonClick);
        }


        private void OnResumeButtonClick()
        {
            Time.timeScale = 1;
            PauseMenuTransform.gameObject.SetActive(false);
            HudTransform.gameObject.SetActive(true);
        }


        private void OnRestartButtonClick()
        {
            Time.timeScale = 1;
            PauseMenuTransform.gameObject.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void OnOptionsButtonClick()
        {
            PauseMenuTransform.gameObject.SetActive(false);
            OptionsMenuTransform.gameObject.SetActive(true);
        }

        private void OnQuitButtonClick()
        {
            Initiate.Fade("MainMenu", Color.black, 5.0f);
            SceneManager.LoadScene("MainMenu");
        }
    }
}