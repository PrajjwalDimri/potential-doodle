﻿namespace UI_Scripts
{
    public class GameSettings
    {
        public float AudioVolume;

        public bool IsFullScreen;
        public int QualityValueIndex;
        public int ResolutionIndex;
        public int GearingMode;
    }
}